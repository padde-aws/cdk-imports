import cdk = require('@aws-cdk/cdk');
import ec2 = require('@aws-cdk/aws-ec2');
export interface AppProps {
    /**
     * The account number to be provisioned in
     *
     * @default 300
     */
    account?: number;
}
export declare class Default extends cdk.Construct {
    readonly vpc: ec2.VpcNetworkRef;
    constructor(parent: cdk.Construct, name: string, props?: AppProps);
}
