import cdk = require('@aws-cdk/cdk');
import ec2 = require('@aws-cdk/aws-ec2')

export interface AppProps {
  /**
   * The account number to be provisioned in
   *
   * @default 300
   */
  account?: number;
}

export class Default extends cdk.Construct {
  public readonly vpc: ec2.VpcNetworkRef;
  constructor(parent: cdk.Construct, name: string, props: AppProps = {}) {
    super(parent, name);

    if (props.account != 300) {
      this.vpc = ec2.VpcNetworkRef.import(this, 'DefaultVPC', {
        vpcId: 'vpc-364b635e',
        availabilityZones: ['eu-central-1a', 'eu-central-1c'],
        publicSubnetIds: ['subnet-41fb212a', 'subnet-41fb212a'],
        // publicSubnetNames: ['Public Subnet 1', 'Public Subnet 2'],
      });
    }
  }
}
