"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cdk = require("@aws-cdk/cdk");
const ec2 = require("@aws-cdk/aws-ec2");
class Default extends cdk.Construct {
    constructor(parent, name, props = {}) {
        super(parent, name);
        if (props.account != 300) {
            this.vpc = ec2.VpcNetworkRef.import(this, 'DefaultVPC', {
                vpcId: 'vpc-364b635e',
                availabilityZones: ['eu-central-1a', 'eu-central-1c'],
                publicSubnetIds: ['subnet-41fb212a', 'subnet-41fb212a'],
            });
        }
    }
}
exports.Default = Default;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLG9DQUFxQztBQUNyQyx3Q0FBd0M7QUFXeEMsTUFBYSxPQUFRLFNBQVEsR0FBRyxDQUFDLFNBQVM7SUFFeEMsWUFBWSxNQUFxQixFQUFFLElBQVksRUFBRSxRQUFrQixFQUFFO1FBQ25FLEtBQUssQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFcEIsSUFBSSxLQUFLLENBQUMsT0FBTyxJQUFJLEdBQUcsRUFBRTtZQUN4QixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxZQUFZLEVBQUU7Z0JBQ3RELEtBQUssRUFBRSxjQUFjO2dCQUNyQixpQkFBaUIsRUFBRSxDQUFDLGVBQWUsRUFBRSxlQUFlLENBQUM7Z0JBQ3JELGVBQWUsRUFBRSxDQUFDLGlCQUFpQixFQUFFLGlCQUFpQixDQUFDO2FBRXhELENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztDQUNGO0FBZEQsMEJBY0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgY2RrID0gcmVxdWlyZSgnQGF3cy1jZGsvY2RrJyk7XG5pbXBvcnQgZWMyID0gcmVxdWlyZSgnQGF3cy1jZGsvYXdzLWVjMicpXG5cbmV4cG9ydCBpbnRlcmZhY2UgQXBwUHJvcHMge1xuICAvKipcbiAgICogVGhlIGFjY291bnQgbnVtYmVyIHRvIGJlIHByb3Zpc2lvbmVkIGluXG4gICAqXG4gICAqIEBkZWZhdWx0IDMwMFxuICAgKi9cbiAgYWNjb3VudD86IG51bWJlcjtcbn1cblxuZXhwb3J0IGNsYXNzIERlZmF1bHQgZXh0ZW5kcyBjZGsuQ29uc3RydWN0IHtcbiAgcHVibGljIHJlYWRvbmx5IHZwYzogZWMyLlZwY05ldHdvcmtSZWY7XG4gIGNvbnN0cnVjdG9yKHBhcmVudDogY2RrLkNvbnN0cnVjdCwgbmFtZTogc3RyaW5nLCBwcm9wczogQXBwUHJvcHMgPSB7fSkge1xuICAgIHN1cGVyKHBhcmVudCwgbmFtZSk7XG5cbiAgICBpZiAocHJvcHMuYWNjb3VudCAhPSAzMDApIHtcbiAgICAgIHRoaXMudnBjID0gZWMyLlZwY05ldHdvcmtSZWYuaW1wb3J0KHRoaXMsICdEZWZhdWx0VlBDJywge1xuICAgICAgICB2cGNJZDogJ3ZwYy0zNjRiNjM1ZScsXG4gICAgICAgIGF2YWlsYWJpbGl0eVpvbmVzOiBbJ2V1LWNlbnRyYWwtMWEnLCAnZXUtY2VudHJhbC0xYyddLFxuICAgICAgICBwdWJsaWNTdWJuZXRJZHM6IFsnc3VibmV0LTQxZmIyMTJhJywgJ3N1Ym5ldC00MWZiMjEyYSddLFxuICAgICAgICAvLyBwdWJsaWNTdWJuZXROYW1lczogWydQdWJsaWMgU3VibmV0IDEnLCAnUHVibGljIFN1Ym5ldCAyJ10sXG4gICAgICB9KTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==